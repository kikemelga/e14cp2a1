class AddCostToArea < ActiveRecord::Migration[5.2]
  def change
    add_column :areas, :cost, :float
  end
end
